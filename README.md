# INTRODUÇÃO DOCKER #

### CONTAINER ###
    - Utiliza/Compartilha os recursos direto do Sistema Operacional (Host);
    - Não precisa de um VM para executar a tarefa, pois ela é multplataforma;
    - Não é uma máquina virtual;
    - Contêm apenas o que precisa para rodar determinado serviço;
    - Extremamente leve;

### VIRTUAL MACHINE ###
    - Define um Sistema Operacional que executa dentro de um outro Sistema Operacional

### HYPERVISOR ###
    - Camada que permite a virtualização

### DOCKER ENGINE ###
    - Definição de docker 

### DOCKER ENGINE ###
    - Camada que permite o gerenciamento de containers

### Compartilhamento ###

##  Run Container MYSQL  ##
    - docker run -d --name=dbserver -e "MYSQL_ROOT_PASSWORD=root" -e "MYSQL_DATABASE=wordpress" mysql
#### -e : Declaração de variavel de ambiente


## Run Container WORDPRESS ## 
    - docker run -d --name=wordpress --link dbserver:mysql -p 8085:80 wordpress

#### --link => serve para linkar um container com outro
#### -p => set a porta de execução do container

## Accessar a aplicação ##
docker exec -it wordpress bash